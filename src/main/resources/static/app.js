class App extends React.Component { 

	constructor(props) {
		super(props);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.state = {scores: []};
	}

	handleSubmit(name) {
		//alert('A name was submitted: ' + name);
		fetch('/api/press?name=' + name)
			.then(res => res.json())
			.then((data) => {
				console.log(JSON.stringify(data));
				this.setState({ scores: data })
			})
		.catch(console.log)	
  }

	render() {
		this.handleSubmit("")
		return (
			<div>
			<NameForm handleSubmit={this.handleSubmit}/>
			<Scores scores={this.state.scores} />
			</div>		);
	}
}

class NameForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: ''};
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) 	{    this.setState({value: event.target.value});  }
  handleSubmit() 		{    		
	event.preventDefault();
	this.props.handleSubmit(this.state.value); 
	}

  render() {
    return (
      <form onSubmit={this.handleSubmit}>        <label>
          Name:
          <input type="text" value={this.state.value} onChange={this.handleChange} />        </label>
        <input type="submit" value="Score me" />
      </form>
    );
  }
}

function Scores(props) {
	return(
	<table>
		<thead><tr><td>Rank</td><td>Name</td><td>Score</td></tr></thead>
		<tbody>
			{props.scores.map(score => 
			<tr key={score.rank}><td>{score.rank}</td><td>{score.name}</td><td>{score.score}</td></tr>
			)}
		</tbody>
	</table>);
}


