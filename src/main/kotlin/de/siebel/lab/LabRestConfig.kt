package de.siebel.lab

import org.springframework.stereotype.Component
import org.glassfish.jersey.server.ResourceConfig
import javax.ws.rs.ApplicationPath

@Component
@ApplicationPath("/api")
class LabRestConfig : ResourceConfig() {
    init {
        register(LabRest::class.java)
    }
}
