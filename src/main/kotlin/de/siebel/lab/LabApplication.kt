package de.siebel.lab

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.glassfish.jersey.server.ResourceConfig
import org.springframework.boot.SpringApplication

@SpringBootApplication
class LabApplication
fun main(args: Array<String>) {
  runApplication<LabApplication>(*args)
}
