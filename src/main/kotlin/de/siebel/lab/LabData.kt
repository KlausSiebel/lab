package de.siebel.lab

import java.util.Date

data class LabData(val rank: Int, val name: String, val score: Int)