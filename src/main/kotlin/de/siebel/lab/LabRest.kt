package de.siebel.lab

import org.springframework.stereotype.Service
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import java.util.Date
import javax.ws.rs.QueryParam
import kotlin.random.Random
import java.util.TreeMap

@Service
@Path("press")

open class LabRest {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
	
    fun processName(@QueryParam("name") name : String) : List<LabData> {
		if (name.isNotBlank()) {
			Scores.scoreToName.put(name, Random.nextInt(-1000, 1000));
		}
		val tmpList : List<LabData> = Scores.scoreToName.toList().sortedWith {
				 il : Pair<String, Int>, ir : Pair<String, Int> -> ir.second - il.second
		} .mapIndexed { idx, it -> LabData(idx + 1, it.first, it.second)	}
		return tmpList;
    }

}

object Scores {
	 val scoreToName = mutableMapOf<String, Int>()
}
