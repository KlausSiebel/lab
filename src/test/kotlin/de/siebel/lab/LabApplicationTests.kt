package de.siebel.lab

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest
class LabApplicationTests {

	@Test
	fun contextLoads() {
		LabRest().processName("Klaus").forEach {
			assert(	it.name.contains("Klaus") && it.score > -1001 && it.score < 1001 && it.rank == 1 )
		}
	}

}
